/**
 * Bài 1
 *
 * Input: Nhập vào số ngày làm của nhân viên: 28
 *
 * Step:
 * +step 1: tạo ra biến chứa số ngày làm của nhân viên : soNgayLam
 * +step 2: tạo ra biên chứa số tiền lương nhân viên của 1 ngày làm : tienLuong1Ngay
 * +step 3: tạo ra biến chứa tổng tiền lương của nhân viên : tongLuong
 * +step 4: gán giá trị cho các biến soNgayLam = 28, tienLuong1Ngay = 100000
 * +step 5: sử dụng phép toán nhân để tính tổng tiền lương nhân viên:
 *              tongLuong = soNgayLam * tienLuong1Ngay
 *
 * Output: Tổng lương của nhân viên làm trong 28 ngày:2800000
 *
 */

var soNgayLam = 28;
var tienLuong1Ngay = 100000;
var tongLuong = soNgayLam * tienLuong1Ngay;
console.log("Tổng tiền lương của nhân viên: ", tongLuong);

/**
 * Bài 2
 *
 * Input: Nhập vào 5 số thực: 5, 10, 15, 20, 25
 *
 * Step:
 * +step 1: tạo ra 5 biến chứa 5 số thực: soThuNhat, soThuHai, soThuBa, soThuTu, soThuNam
 * +step 2: tạo ra biến chứa tổng 5 số thực: tongSoThuc
 * +step 3: tạo ra biến chứa giá trị trung bình của 5 số thực: soTB
 * +step 4: gán giá trị cho 5 số thực soThuNhat = 5, soThuHai = 10, soThuBa = 15, soThuTu = 20, soThuNam = 25
 * +step 5: dùng phép tính cộng tính tổng 5 số thực
 *                  tongSoThuc = soThuNhat + soThuHai + soThuBa + soThuTu + soThuNam
 * +step 6: dùng phép tính chia tính ra giá trị trung bình của 5 số thực
 *                  soTB = tongSoThuc / 5
 *
 *
 * Output: Giá trị trung bình của 5 số thực là 15
 *
 */

var soThuNhat = 5;
var soThuHai = 10;
var soThuBa = 15;
var soThuTu = 20;
var soThuNam = 25;
var tongSoThuc = soThuNhat + soThuHai + soThuBa + soThuTu + soThuNam;
var soTB = tongSoThuc / 5;
console.log("Giá trị trung bình của 5 số thực là: ", soTB);

/**
 * Bài 3
 *
 * Input: Nhập vào số tiền người dùng muốn đổi từ USD sang VND: 100
 *
 * Step:
 * +step 1: tạo ra biến chứa số tiền người dùng muốn đổi từ USD sang VND: soTienDoi
 * +step 2: tạo ra biến chứa giá tiền 1 USD đổi sang VND: gia1DoLa
 * +step 3: tạo ra biến chứa tổng số tiền nhận được của người dùng sau khi đổi từ USD sang VND: tongSoTien
 * +step 4: gán giá trị cho các biến soTienDoi = 100, gia1DoLa = 23500
 * +step 5: dùng phép tính nhân để tính ra tổng số tiền người dùng đổi được
 *          tongSoTien = soTienDoi * gia1DoLa
 *
 * Output: Số tiền người đổi nhận được: 2350000
 *
 */

var soTienDoi = 100;
var gia1DoLa = 23500;
var tongSoTien = soTienDoi * gia1DoLa;
console.log("Số tiền người đổi nhận được là: ", tongSoTien);

/**
 *
 * Bài 4
 *
 * Input: Nhập vào chiều dài: 10 và chiều rộng: 5 của hình chữ nhật
 *
 * Step:
 * +step 1: tạo ra biến chứa chiều dài và chiều rộng của hình chữ nhật: chieuDai, chieuRong
 * +step 2: tạo ra biến chứa chu vi và diện tích của hình chữ nhật: chuVi, dienTich
 * +step 3: gán giá trị cho các biến : chieuDai = 10, chieuRong = 5
 * +step 4: Áp dụng công thức để tính các giá trị chu vi và diện tích của hình chữ nhật
 *                   chuVi = (chieuDai + chieuRong) * 2
 *                   dienTich = chieuDai * chieuRong
 *
 *
 * Output: Chu vi của hình chữ nhật là 30
 *         Diện tích của hình chữ nhật là 50
 *
 */

var chieuDai = 10;
var chieuRong = 5;
var chuVi = (chieuDai + chieuRong) * 2;
console.log("Chu vi của hình chữ nhật là: ", chuVi);
var dienTich = chieuDai * chieuRong;
console.log("Diện tích của hình chữ nhật là: ", dienTich);

/**
 * Bài 5
 *
 * Input: Nhập vào số có 2 chữ số: 56
 *
 * Step:
 * +step 1: tạo ra biến chứa số có 2 chữ số: so
 * +step 2: tạo ra biến chứa số hàng chục: chuc, số hàng đơn vị: donVi
 * +step 3: tạo ra biến chứa tổng 2 ký số của số vừa nhập vào: tong
 * +step 4: gán giá trị cho số có 2 chữ số so = 56
 * +step 5: lấy số hàng chục của số vừa nhập
 *          chuc = Math.floor(so/10)
 * +step 6: lấy số hàng đơn vị của số vừa nhập
 *          donVi = so % 10
 * +step 7: dùng phép tính cộng tính tổng 2 ký số
 *          tong = chuc + donVi
 *
 *
 * Output: Tổng 2 ký số của số vừa nhập vào là 11
 *
 *
 */

var so = 56;
var chuc = Math.floor(so / 10);
var donVi = so % 10;
var tong = chuc + donVi;
console.log("Tổng 2 ký số của số vừa nhập vào là: ", tong);
